# SnailBar

## 项目介绍
本项目是基于开源项目[SnailBar](https://github.com/android-cjj/SnailBar) 进行harmonyos化的移植和开发的。  
用来展示一个较为炫酷的自定义动态进度条

#### 项目名称：SnailBar
#### 所属系列：harmonyos的第三方组件适配移植

#### 功能：一个适用于harmonyos的自定义进度条
#### 项目移植状态：完全移植
#### 调用差异：无
#### 原项目GitHub地址：https://github.com/android-cjj/SnailBar

## 支持功能
- 进度条自动拖动展示
- 进度条速度变化时，状态文字实时变化
- 进度条的自定义拖动块动画正常显示 
- 进度条背景颜色正常显示 

## 安装教程

#### 方案一  
 
```
  //核心引入 
  implementation project(':snailbar')
```
#### 方案二
项目根目录的build.gradle中的repositories添加：
```
mavenCentral()
```

module目录的build.gradle中dependencies添加：
```
implementation 'com.gitee.ts_ohos:snailbar:1.0.0'
```

## 使用说明

#### 代码使用  
MainAbilitySlice，UI页面，用来显示自定义控件和设置
````java
    private SnailBar snailBar;
        snailBar = (SnailBar) findComponentById(ResourceTable.Id_seekBar);

        snailBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                InnerEvent message = InnerEvent.get();
                PacMap bundle = new PacMap();

                float pro = seekBar.getProgress();
                float num = seekBar.getMax();
                float result = (pro / num) * 100;

                bundle.putFloatValue("per", result);
                message.setPacMap(bundle);
                message.eventId = 0;

                handler.sendEvent(message);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

    int len = 0;
    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            LogUtil.error("mHandler", "eventId: " + event.eventId);
            switch (event.eventId) {
                case 0:
                    break;
                case 1:
                    if (snailBar.getProgress() < 100) {
                        if (snailBar.getProgress() < 20) {
                            len += 2;
                            handler.sendEvent(1, 500);
                            seekbarStatus.setText("Normal download speed.");
                        } else if (snailBar.getProgress() > 21 && snailBar.getProgress() < 26) {
                            len += 1;
                            handler.sendEvent(1, 1000);
                            seekbarStatus.setText("Sundden speed down or disconnet...");
                        } else {
                            len += 2;
                            handler.sendEvent(1, 50);
                            seekbarStatus.setText("Sundden speed up.");
                        }
                        snailBar.setProgressValue(len);

                    }
                    break;
            }
        }
    };

````
自定义SnailBar类，用来设置基本属性
````java
    private void init(Context context) {
        this.setMaxValue(100);
        //计算方式在ohos有问题，因此改为固定大小。
        //this.setStep(dip2px(getContext(), 20));
        this.setStep(2);

        //不支持.9图，因此改为颜色背景。
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setAlpha(100);
        shapeElement.setShape(50);
        shapeElement.setBounds(20, 20, 20, 20);
        shapeElement.setCornerRadius(120);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        this.setBackground(shapeElement);

        FrameAnimationElement frameAnimationElement = new FrameAnimationElement(getContext(), ResourceTable.Graphic_animation_element);
        //加载动画资源，生成动画对象。
        //创建播放动画的组件
        this.setThumbElement(frameAnimationElement);
        //开始播放动画
        frameAnimationElement.start();

        //不支持.9图，因此改为颜色背景。
        ShapeElement progressElement = new ShapeElement();
        progressElement.setAlpha(100);
        progressElement.setRgbColor(RgbColor.fromArgbInt(Color.GREEN.getValue()));
        this.setProgressElement(progressElement);

        //int padding = dip2px(getContext(), (float) 20);
        int padding = 80;
        this.setPadding(padding, padding, padding, padding);

    }
````

## 效果展示
<img src="./image/main.gif">

## License

MIT License

Copyright (c) 2015 android-cjj

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
