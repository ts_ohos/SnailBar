package com.ohos.snailbar.slice;

import com.ohos.snailbar.ResourceTable;
import com.ohos.library.LogUtil;
import com.ohos.library.SnailBar;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.utils.PacMap;

public class MainAbilitySlice extends AbilitySlice {
    private SnailBar snailBar;
    private Text seekbarPercent, seekbarStatus;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        snailBar = (SnailBar) findComponentById(ResourceTable.Id_seekBar);
        seekbarPercent = (Text) findComponentById(ResourceTable.Id_seekbar_percent);
        seekbarStatus = (Text) findComponentById(ResourceTable.Id_seekbar_status);

        snailBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                InnerEvent message = InnerEvent.get();
                PacMap bundle = new PacMap();

                float pro = snailBar.getProgress();
                float num = snailBar.getMax();
                float result = (pro / num) * 100;

                bundle.putFloatValue("per", result);
                message.setPacMap(bundle);
                message.eventId = 0;

                handler.sendEvent(message);
            }

            @Override
            public void onTouchStart(Slider slider) {

            }

            @Override
            public void onTouchEnd(Slider slider) {

            }
        });

        handler.postTask(new Runnable() {
            @Override
            public void run() {
                handler.sendEvent(1);
            }
        }, 2000);


    }

    int len = 0;
    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner()) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            LogUtil.error("mHandler", "eventId: " + event.eventId);
            switch (event.eventId) {
                case 0:
                    break;
                case 1:
                    if (snailBar.getProgress() < 100) {
                        if (snailBar.getProgress() < 20) {
                            len += 2;
                            handler.sendEvent(1, 500);
                            seekbarStatus.setText("Normal download speed.");
                        } else if (snailBar.getProgress() > 21 && snailBar.getProgress() < 26) {
                            len += 1;
                            handler.sendEvent(1, 1000);
                            seekbarStatus.setText("Sundden speed down or disconnet...");
                        } else {
                            len += 2;
                            handler.sendEvent(1, 50);
                            seekbarStatus.setText("Sundden speed up.");
                        }
                        snailBar.setProgressValue(len);

                    }
                    break;
            }
        }
    };

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
