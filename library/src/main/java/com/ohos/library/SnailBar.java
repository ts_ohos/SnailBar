package com.ohos.library;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Slider;
import ohos.agp.components.element.FrameAnimationElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class SnailBar extends Slider {

    public SnailBar(Context context) {
        this(context, null);
        init(context);
    }

    public SnailBar(Context context, AttrSet attrs) {
        this(context, attrs, "0");
        init(context);
    }

    public SnailBar(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.setMaxValue(100);
        //计算方式在ohos有问题，因此改为固定大小。
        //this.setStep(dip2px(getContext(), 20));
        this.setStep(2);

        //不支持.9图，因此改为颜色背景。
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setAlpha(100);
        shapeElement.setShape(50);
        shapeElement.setBounds(20, 20, 20, 20);
        shapeElement.setCornerRadius(120);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        this.setBackground(shapeElement);

        FrameAnimationElement frameAnimationElement = new FrameAnimationElement(getContext(), ResourceTable.Graphic_animation_element);
        //加载动画资源，生成动画对象。
        //创建播放动画的组件
        this.setThumbElement(frameAnimationElement);
        //开始播放动画
        frameAnimationElement.start();

        //不支持.9图，因此改为颜色背景。
        ShapeElement progressElement = new ShapeElement();
        progressElement.setAlpha(100);
        progressElement.setRgbColor(RgbColor.fromArgbInt(Color.GREEN.getValue()));
        this.setProgressElement(progressElement);

        //int padding = dip2px(getContext(), (float) 20);
        int padding = 80;
        this.setPadding(padding, padding, padding, padding);

    }

    @Override
    public boolean isBoundToWindow() {
        return super.isBoundToWindow();
    }

    public int dip2px(Context context, float dpValue) {
        final float scale = context.getResourceManager().getDeviceCapability().screenDensity;
        return (int) (dpValue * scale + 0.5f);
    }


}
